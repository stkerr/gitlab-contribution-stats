Create a CSV file listing all of your contributions to GitLab!

This builds off of the [Contributions Event API](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events)

# To use

1. Create a file `.env` with the contents:
   ```
   GITLAB_API_TOKEN="<your token>"
   GITLAB_USER_ID=<your user ID>
   ```
1. Install the Python requirements file:
   - `pip install -r requirements.txt`
1. Run it!
   - `python main.py`
1. View the results
   - A file called `contributions.csv` will be created that has all your contributions in it.
   - Experiment with the data in a spreadsheet or graphing program!


# Data
When run on 2021-06-08, the CSV file had these columns:

|id|project_id|action_name|target_id|target_iid|target_type|author_id|target_title|created_at|author|push_data|author_username|
|-|-|-|-|-|-|-|-|-|-|-|-|

# Graph
The script creates a radar chart to show what sort of distribution between different event types there are.

![example graph](example.png)
