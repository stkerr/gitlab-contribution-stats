import os
import base64
import datetime
from dotenv import load_dotenv
import json
import requests

load_dotenv()

GITLAB_API_TOKEN = os.getenv('GITLAB_API_TOKEN')
GITLAB_USER_ID = os.getenv('GITLAB_USER_ID')

GITLAB_BASE_URL = "https://gitlab.com/api/v4"
HEADERS = {
    'PRIVATE-TOKEN': GITLAB_API_TOKEN
}

import csv
with open("contributions.csv","w", newline='') as csvfile:
    contributions = []

    page_count = 0
    while True:
        page_count = page_count + 1

        print("Getting contributions page %d" % page_count)

        PARAMS = {
            'per_page': '200',
            'page': '%s' % page_count
        }

        response = requests.get(GITLAB_BASE_URL+"/users/%s/events" % GITLAB_USER_ID,
            headers=HEADERS,
            params=PARAMS
        )
        if json.loads(response.text) == []:
            break


        items = json.loads(response.text)
        contributions.extend(items) # extend not append, to join lists


    print("Done! Page %s" % page_count)

    writer = csv.writer(csvfile)
    writer.writerow(contributions[0].keys())
    for i in contributions:
        writer.writerow(i.values())

# Create a radar chart
import pandas as pd
import plotly.express as px
data = pd.read_csv('contributions.csv')

df = pd.DataFrame(data)
table = df.pivot_table(index='action_name', values=['id'], aggfunc='count')
polar = pd.DataFrame(dict(
    r=list(table['id']),
    theta=list(table.index)
))

fig = px.line_polar(polar, r='r', theta='theta', line_close=True)
fig.show()
fig.write_html('contributions.html', auto_open=True)